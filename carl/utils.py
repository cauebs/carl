from wrapt import decorator


@decorator
def empty_args(f, _, args, kwargs):
    if len(args) == 1 and not kwargs and callable(args[0]):
        return f()(args[0])
    else:
        return f(*args, **kwargs)


@decorator
def printer(f, _, args, kwargs):
    print(f(*args, **kwargs))
